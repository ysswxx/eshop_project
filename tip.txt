1.	Eclipse导入maven项目使其与本地创建的项目目录结构一致（已解决）
2.	如何访问WEB-INF下的静态文件（解决）
3.	web.xml中配置springmvc的url-parttern中  / 和  /* 的区别（已查阅）
4.	类似引进某种三方jar出现某些类class not found警告或错误（序列化与反序列化导致），
	检查项目是否重新clean外，可以在对应业务层引入此依赖解决。
5.	待完成（已完成，条目6）：由于dubbo配置文件需要经常变动配置，而地址也需要频繁改动，不如将dubbo地址放到配置文件中，
	抽离出来避免不同开发人员间频繁改动	
6.	dubbo provider启动时，加载spring配置文件报错：
	Could not resolve placeholder 'xxx.xxx' in string value "${xxx.xxx} （已解决，重要）
7.  （待深入学习）markdown语法编辑器、MyBtais生成代码的使用、代码Generator工具、zkServer.sh 命令、nginx 命令
8.	图片上传失败（文件夹权限问题）、上传成功但大小为0（ftpClient没有设置为被动模式）
9.  种种原因导致当前HEAD在origin/master分支上，但是master分支在历史记录里
	解决：---先将当前的提交到远程----新建分支----切换master分支----合并当前到master	
10. private final static Logger LOGGER = LoggerFactory.getLogger(TbItemController.class);
11. jsonp 请求响应格式
12. 查询数据中的字段在数据库中是大文本text数据类型时，需要指定查询方法为 ...WithBlob(),否则不会将这个字段的值查询出来
	为了查询效率最大化来考虑的。
13.	Restful 请求地址对应的控制器  @RequestMapping("/resource/{id}")  @PathVariable Integer id
14.	一级菜单pid使用0而不是null的好处，在加载第一层级时与加载其他层级时，sql不用做区分，因为pid为null 不能直接传null 而是   pid is not null 	
15. web项目在git上的上传和下载相关操作   保证上传与下载之间结构一致（参见word说明）
16.	dubbo consumer项目启动过程中报错：no provider available for the service
           原因：dubbo中未启动该服务（未配置），但是当前消费者有依赖，所以报错
17. 后台同步缓存操作复杂（内容的更新中大广告数据更新只是其中一种情况，分类的更新在树中遍历数据异常复杂）--考虑使用统一的刷新缓存机制   
18. 如何在一个主机上启动多个tomcat(已记载到CSDN博客)      
19. 强制关闭进程  kill -9 [进程号]
20. solr添加列  solrhome/collection1/conf/schema.xml中配置
21. git支持覆盖选项 overwrite
22. 从git上导入新模块目录不统一，可以删除全部（不在磁盘删除），在重新导入maven工程
23. eclipse中所有maven项目都报错缺少jar，可以将setting设置界面中还原所有设置解决
24. 快速生成遍历集合代码   输入fore 然后alt+/
 	同屏切分代码  ctrl+shift+- ，还原同理
	查看类继承树   f4
	
webmanage 	8080
item 		8081
portal 		8082
search 		8083
passport    8084
cart		8085
order 		8086

25. maven父工程下子模块pom.xml中配置依赖时，会报缺少版本号错误：
	Project build error: 'dependencies.dependency.version' for org.springframework:spring-webmvc:jar is missing.
	解决办法：在父工程的pom.xml中的<dependencies>外层添加<dependencyManagement>标签
	
26. 关于get方式提交中文乱码问题的解决方式 https://blog.csdn.net/u012564085/article/details/80003283
27. 当前项目需要通过HttpClient请求其他项目的控制器时，如果参数使用对象封装传输过去，可以考虑json格式数据
	对应的控制器使用“@RequestBody 类  类名”方式接收
	@ResponseBody:表示将响应的对象转换为json存放到响应体中
	@RequestBody:表示将请求体中的json流转换为对象

28.	本地环境下  使用Debug模式启动项目可以实现自动编译部署
29. 去除html页面中GET《 http://localhost:8080/favicon.ico 404 (Not Found)》
30.	jsonp请求，返回正确格式json数据仍报错：Uncaught SyntaxError: Unexpected token ':'
	原因：后台没有设置回调函数callback，即返回的数据没有作为回调函数的实参
31.	git stash可以将当前修改过但暂时还不能提交的代码保存，并且可以恢复到当前分支或者其他分支上		
	当想要从stash中恢复时保证当前的文件改动与stash中存储的改动没有冲突，最好是都提交过在来从stash中取出内容	
32.	业务中需要特殊的传输对象（在pojo基础上的类）应尽量少的改动基础pojo,更应该在本业务上新建pojo，防止数据库结构发生改变需要大量改动基础pojo
33.	包装类的equals慎用，只有当参数为Long类（instanceof）时，才会比较数值
		Integer a = 8;
		Long b = 8L;
		System.out.println(b.equals(a));//false
		System.out.println(b.compareTo(a.longValue())==0);//true	
34. dubbo.xml配置注解扫描的时候，不能使用通配符
35. dubbo.xml配置引入其他配置文件时也不能使用通配符，在使用assembly打包插件打包后，启动会报错
	cause by:JAR entry META-INF/spring/../../ not found in C:\Users\孙哈哈\Desktop\eshop_service_impl-0.0.1-SNAPSHOT\lib\eshop_service_impl-0.0.1-SNAPSHOT.jar
36. 连接dubbo服务时，响应超时，数据没有加载出来，需要配置dubbo服务超时时间（默认是1秒），单位毫秒
	服务端配置： <dubbo:provider  timeout="5000"/>
37. 本地hosts可以配置域名解析，如果需要在IP地址后添加端口号可以使用nginx来配置
	server { 
    listen  api.mydomain.com:80; 
    server_name api.mydomain.com; 
    location/{ 
    proxy_pass http://127.0.0.1:8000; 
    } 
}	
			