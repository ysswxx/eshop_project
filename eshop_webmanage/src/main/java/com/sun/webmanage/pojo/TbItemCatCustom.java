package com.sun.webmanage.pojo;

import java.util.List;

import com.sun.webmanage.model.TbItemCat;

public class TbItemCatCustom extends TbItemCat{

	private List<TbItemCatCustom> child;

	public List<TbItemCatCustom> getChild() {
		return child;
	}

	public void setChild(List<TbItemCatCustom> child) {
		this.child = child;
	}
	
	
}
