package com.sun.webmanage.service;

import java.util.List;

import com.sun.commons.pojo.EasyUITreeData;

public interface TbItemCatService {

	List<EasyUITreeData> listEasyUITreeData(long pid);
	
	boolean setCatPath();
}
