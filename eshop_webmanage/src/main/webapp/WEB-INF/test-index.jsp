<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<% String basepath = request.getContextPath(); %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>欢迎页</title>
<link rel="stylesheet" href="<%=basepath %>/css/base.css">
<link rel="stylesheet" href="<%=basepath %>/layui/css/layui.css">
<script src="<%=basepath %>/layui/layui.js"></script>
<script src="<%=basepath %>/js/js-utils/ajaxrequest.js"></script>
<script src="<%=basepath %>/js/js-utils/layeralert.js"></script>
<script src="<%=basepath %>/js/js-utils/menutree.js"></script>
</head>
<body>
<h1>Welcome Eshop project</h1>
<form action="upload" method="post" encType="multipart/form-data">
<input type="file" name="file" />
<input type="submit" value="上传"/>
</form>
</body>
<script>
//一般直接写在一个js文件中
layui.use(['layer', 'form'], function(){
  var layer = layui.layer
  ,form = layui.form;
  
  layer.msg('Hello World');
});
</script> 
</html>