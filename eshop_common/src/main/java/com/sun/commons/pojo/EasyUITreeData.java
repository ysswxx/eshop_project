package com.sun.commons.pojo;

import java.io.Serializable;
import java.util.List;

public class EasyUITreeData implements Serializable{

	private Long id;//节点id
	private String text;//显示的节点文本
	private String state;//节点状态, 'open' 或者 'closed', 默认是 'open'. 当设置为 'closed', 节点所有的子节点将从远程服务器站点加载
	private Boolean checked;//指明检查节点是否选中.
	private String attributes;//可以添加到节点的自定义属性
	private List<EasyUITreeData> children;//一个节点数组,定义一些子节点

	public EasyUITreeData(){
		this.checked = false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	public String getAttributes() {
		return attributes;
	}

	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}

	public List<EasyUITreeData> getChildren() {
		return children;
	}

	public void setChildren(List<EasyUITreeData> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return "EasyUITreeData [id=" + id + ", text=" + text + ", state=" + state + ", checked=" + checked
				+ ", attributes=" + attributes + ", children=" + children + "]";
	}

	// easy ui 树结构 model类

}
