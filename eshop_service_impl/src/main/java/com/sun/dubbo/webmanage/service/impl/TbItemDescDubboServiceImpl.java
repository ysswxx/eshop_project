package com.sun.dubbo.webmanage.service.impl;

import javax.annotation.Resource;

import com.sun.dubbo.webmanage.service.TbItemDescDubboService;
import com.sun.webmanage.mapper.TbItemDescMapper;
import com.sun.webmanage.model.TbItemDesc;
import com.sun.webmanage.model.TbItemDescExample;

public class TbItemDescDubboServiceImpl implements TbItemDescDubboService{

	@Resource
	private TbItemDescMapper tbItemDescMapper;

	@Override
	public TbItemDesc selectTbItemDescByItemId(long item_id) {
		TbItemDescExample example = new TbItemDescExample();
		example.createCriteria().andItemIdEqualTo(item_id);
		return tbItemDescMapper.selectByExampleWithBLOBs(example).get(0);
	}

}
